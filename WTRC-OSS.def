################################### Water Cooling Systems ##################################
##                               OSS - Optical Oxygen Sensor 
##
############################ Version: 1.0             ######################################
# Author:	Thomas Fay 
# Date:		2018-11-22
# Version:  v1.0

############################
#  STATUS BLOCK
############################ 
define_status_block()

#for OPI visualisation
add_analog("ScaleLOW","REAL",                        PV_DESC="Scale LOW",                  PV_EGU="ug/L")
add_analog("ScaleHIGH","REAL",                       PV_DESC="Scale HIGH",                 PV_EGU="ug/L")

#Status
add_analog("Status","BYTE",		                    PV_DESC="Oxygen Content Status",       PV_EGU="")

#Transmitter value
add_analog("MeasValue","REAL",						 PV_DESC="Measured Value",             PV_EGU="ug/L")

#Feedback
add_analog("FB_Limit_HIHI","REAL",                   PV_DESC="Feedback Limit HIHI",        PV_EGU="ug/L")
add_analog("FB_Limit_HI","REAL",                     PV_DESC="Feedback Limit HI",          PV_EGU="ug/L")
add_analog("FB_Limit_LO","REAL",                     PV_DESC="Feedback Limit LO",          PV_EGU="ug/L")
add_analog("FB_Limit_LOLO","REAL",                   PV_DESC="Feedback Limit LOLO",        PV_EGU="ug/L")

#Alarm signals
add_major_alarm("HiHiAlarm",             "High High Trip",               PV_ZNAM="NominalState")
add_minor_alarm("HiAlarm",               "High Alarm",                   PV_ZNAM="NominalState")
add_major_alarm("LoLoAlarm",             "Low Low Trip",                 PV_ZNAM="NominalState")
add_minor_alarm("LoAlarm",               "Low Alarm",                    PV_ZNAM="NominalState")
add_major_alarm("SensorFault",           "Sensor Fault",                 PV_ZNAM="NominalState")

############################
#  COMMAND BLOCK
############################ 
define_command_block()

# No commands
############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Limits
add_analog("P_Limit_HIHI","REAL",                    PV_DESC="Limit HIHI",                  PV_EGU="ug/L")
add_analog("P_Limit_HI","REAL",                      PV_DESC="Limit HI",                    PV_EGU="ug/L")
add_analog("P_Limit_LO","REAL",                      PV_DESC="Limit LO",                    PV_EGU="ug/L")
add_analog("P_Limit_LOLO","REAL",                    PV_DESC="Limit LOLO",                  PV_EGU="ug/L")
